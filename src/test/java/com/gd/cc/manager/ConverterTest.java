package com.gd.cc.manager;

import com.gd.cc.provider.DummyRatesProvider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.money.UnknownCurrencyException;
import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class ConverterTest {

    @InjectMocks
    private Converter converter;
    @Mock
    private DummyRatesProvider ratesProvider;

    @Test
    public void checkForUnknownCodesTest () {
         assertThat (converter.checkForUnknownCodes("USD", "RUR", "PLN", "EUR"), is(empty()));
         assertThat (converter.checkForUnknownCodes("USD", "RUR", "PLN", "UNKNWN"), contains( "UNKNWN"));
    }

    @Test
    public void convertTest () {
        when (ratesProvider.convertationRate(eq("RUR"), eq ("USD"))).thenReturn(new BigDecimal(10d));
        assertThat (converter.convert(new BigDecimal(10), "RUR", "USD"), is(new BigDecimal(100)));
    }

    @Test(expected = UnknownCurrencyException.class)
    public void converterExceptionTest (){
        when (ratesProvider.convertationRate(eq("RUR"), eq ("USD"))).thenReturn(new BigDecimal(10d));
        converter.convert(new BigDecimal(10), "UNKNWN", "USD");
    }
}
