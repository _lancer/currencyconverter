package com.gd.cc.provider;

import javafx.util.Pair;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Random;

import static java.math.BigDecimal.ROUND_HALF_DOWN;

@Component
public class DummyRatesProvider implements RatesProvider {

    private Random random = new Random();
    private HashMap<Pair<String, String>, BigDecimal> ratesMap = new HashMap<>();

    /**
     * returns rate from map
     * if no such rate creates one with random value rounded to .000
     *
     * @param fromCurrency currency convert from
     * @param toCurrency currency convert to
     * @return convertation rate
     */
    public BigDecimal convertationRate(String fromCurrency, String toCurrency) {
        Pair <String, String> currencyPair = new Pair<>(fromCurrency, toCurrency);
        BigDecimal rate = ratesMap.get(currencyPair);
        if (rate == null) {
            return generateAndApplyRate(currencyPair);
        }
        return rate;
    }

    public BigDecimal generateAndApplyRate (Pair <String, String> currencyPair) {

        BigDecimal rate = new BigDecimal(random.nextDouble()).setScale(3, ROUND_HALF_DOWN);

        ratesMap.put (currencyPair, rate);
        return rate;
    }


}
