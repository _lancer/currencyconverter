package com.gd.cc.provider;

import java.math.BigDecimal;

public interface RatesProvider {

    BigDecimal convertationRate (String fromCurrency, String toCurrency);
}
