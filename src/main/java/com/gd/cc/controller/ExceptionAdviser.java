package com.gd.cc.controller;

import com.gd.cc.domain.ErrorDetails;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.money.UnknownCurrencyException;
import java.util.Date;

@ControllerAdvice
@RestController
public class ExceptionAdviser extends ResponseEntityExceptionHandler {

    @ExceptionHandler(UnknownCurrencyException.class)
    public final ResponseEntity<ErrorDetails> unknownCurrencyException(UnknownCurrencyException ex, WebRequest request) {
        ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(),
                request.getDescription(false));
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }
}
