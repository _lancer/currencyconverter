package com.gd.cc.controller;

import com.gd.cc.domain.ConvertRequest;
import com.gd.cc.domain.ConvertResponse;
import com.gd.cc.domain.ErrorDetails;
import com.gd.cc.manager.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.money.UnknownCurrencyException;
import java.math.BigDecimal;
import java.util.Date;

import static java.math.BigDecimal.ROUND_HALF_DOWN;

@RestController
public class ConverterController {

    @Autowired
    private Converter converter;

    @RequestMapping(path = "/convert", method = RequestMethod.POST)
    public ResponseEntity<ConvertResponse> convert(@RequestBody ConvertRequest request) {
        return wrapToResponse (converter.convert(request.getAmount(), request.getFromCurrency(), request.getToCurrency()));
    }

    public ResponseEntity<ConvertResponse> wrapToResponse (BigDecimal amount) {
        ConvertResponse response = new ConvertResponse();
        response.setAmount(amount);
        return new ResponseEntity<ConvertResponse>(response, HttpStatus.OK);
    }

}
