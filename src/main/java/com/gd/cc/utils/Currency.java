package com.gd.cc.utils;

import javax.money.CurrencyQueryBuilder;
import javax.money.CurrencyUnit;
import javax.money.Monetary;
import java.util.Collection;

public class Currency {
    public static Collection<CurrencyUnit> echoCurrencyCodes (String... currencyCodes) {
        return Monetary.getCurrencies(CurrencyQueryBuilder.of().setCurrencyCodes(currencyCodes).build());
    }
}
