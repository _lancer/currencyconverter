package com.gd.cc.domain;

import java.math.BigDecimal;

public class ConvertResponse extends ErrorDetails {

    private BigDecimal amount;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
