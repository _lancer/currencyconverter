package com.gd.cc.domain;

import java.math.BigDecimal;

public class ConvertRequest {

    private BigDecimal amount;
    private String fromCurrency;
    private String toCurrency;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    /**
     *  @return JSR 354 code
     */
    public String getFromCurrency() {
        return fromCurrency;
    }

    /**
     * @param fromCurrency JSR 354 code
     */
    public void setFromCurrency(String fromCurrency) {
        this.fromCurrency = fromCurrency;
    }
    /**
     * @return JSR 354 code
     */
    public String getToCurrency() {
        return toCurrency;
    }
    /**
     * @param toCurrency JSR 354 code
     */
    public void setToCurrency(String toCurrency) {
        this.toCurrency = toCurrency;
    }
}
