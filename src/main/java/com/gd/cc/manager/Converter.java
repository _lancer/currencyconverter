package com.gd.cc.manager;

import com.gd.cc.provider.RatesProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.money.CurrencyUnit;
import javax.money.UnknownCurrencyException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static com.gd.cc.utils.Currency.echoCurrencyCodes;


@Component
public class Converter {

    @Autowired
    private RatesProvider ratesProvider;

    public BigDecimal convert (BigDecimal amount, String fromCurrency, String toCurrency) {
        List<String> unknownCurrencies = checkForUnknownCodes (fromCurrency, toCurrency);
        if (unknownCurrencies.size() > 0) {
            throw new UnknownCurrencyException("Can not convert unknown currencies: " + unknownCurrencies);
        }
        return amount.multiply(ratesProvider.convertationRate(fromCurrency, toCurrency));
    }

    /**
     * check currency codes
     *
     * @param currencyCodes currency codes to check
     * @return list of unknown currencies
     */
    public List<String> checkForUnknownCodes (String... currencyCodes) {
        Collection <String> echoCurrencyCodes =
                echoCurrencyCodes(currencyCodes).stream().map(CurrencyUnit::getCurrencyCode).collect(Collectors.toList());
        return Arrays.stream(currencyCodes)
                .filter( code -> !echoCurrencyCodes.contains(code)).collect(Collectors.toList());
    }
}
